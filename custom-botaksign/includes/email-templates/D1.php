<table id="header-infor" style="border-collapse: collapse; width: 100%;" width="100%">
    <tbody><tr>
            <td style="width:100%;padding-top:30px;" class="bill-to-th" align="left"><img style="width:100%" class="confim" src="https://botaksign.com/wp-content/plugins/custom-botaksign/assets/images/D1.png"></td>
        </tr>
    </tbody></table>
<div id="infor" style="margin-top: -30px; width: 100%; height: auto; margin-right: 25px; margin-left: 25px;">
    <span class="info-title" style="color: #27793d; display: block; font-family: segoe-bold; font-size: 16pt;"><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></span><br>
    <span class="info-subtext" style="font-family: Myriad-Pro-Semibold; color: #231f20; font-size: 15pt;">Thank you for signing up with us!</span>
</div>
<div class="text-content-3" style="width: 95%;padding-top: 25px;padding-left: 25px;padding-right: 25px;">
    <span class="order-link-text-none" style="color: #231f20; font-size: 15pt;">You may access your account area, where you can view your orders, update your
        particulars and more <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="order-link-here" style="color: #fcaf17; font-family: segoe-bold; font-size: 15pt;"> HERE.</a> Below is your automatically generated password
        (we recommend changing it to something you can remember easier).</span><br><br>
    <span class="order-link-text" style="color: #15171b; font-family: segoe-bold; font-size: 15pt;"> Password: </span> <span class="order-link-text-1" style="color: #27793d; font-family: segoe-bold; font-size: 15pt;"> <?php echo $user_pass; ?></span>
</div>
<div id="tex-info-2" style="width: 100%; height: 142px; padding-top: 20px; margin-top: 20px; background: #fffbcc; text-align: center;"><span class="tex-info-2-title" style="color: #27793d; display: block; font-family: segoe-bold; font-size: 17pt;">Take a look around our site and see what we have to offer!</span>
    <table style="border-collapse: collapse;">
        <tbody><tr>
                <td style="padding-left:100px;padding-top:5px;"><img src="https://botaksign.cmsmart.net/wp-content/plugins/custom-botaksign/assets/images/D1-img2.png"></td><td>
                </td>
                <td><div class="tex-info-2-button" style="width: 200px; padding-top: 15px; padding-bottom: 15px; color: #ffffff; font-family: segoe-bold; background: #a3cf62; margin-left: 28px;font-size: 15pt;">
                        <a href="#" style="display: block; color: #ffffff; text-decoration: none;">GET STARTED</a>
                    </div></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="order-note" style="width: 95%;margin-top: 20px;padding-left: 25px;padding-right: 25px;padding-bottom: 35px;text-align: left;color: #231f20;font-size: 15pt;"><span class="order-note-text">In the meantime, if you have any enquiries, feel free to drop us an email at <a href="#" class="order-note-link" style="color: #27793d;font-family: segoe-bold;">info@botaksign.com.sg</a>,  or give us a call at 
        <a href="#" class="order-note-link" style="color: #27793d;/* display: block; */font-family: segoe-bold;">6286 2298</a></span><br><br><span class="order-note-text">We look forward to working with you!</span></div><div id="thanks-div" style="width: 95%;text-align: right;padding-left: 25px;padding-right: 25px;"><span class="thank-name" style="color: #231f20; display: block; font-size: 17pt;">Cheers,</span><br><span class="thank-title" style="color: #27793d; display: block; font-family: segoe-bold; font-size: 17pt;">Botak Sign</span></div>
<div id="line-border" style="border-bottom: 1px solid #a3cf62; width: 200pt; margin: 0px auto; margin-top: 10px; padding-top: 20px;"></div>