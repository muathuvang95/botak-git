<div id="footer_social" style="width:65%;text-align:center;padding-left:17.5%;padding-right:17.5%;padding-top:30pt;padding-bottom:30pt; text-align: center">
    <table style="border-collapse:collapse;display: inline-block;margin: 0 auto;">
        <style type="text/css">
            @media only screen and (min-width: 992px) {
                #footer_social table>tbody>tr td {
                    padding-left: 25px!important;
                    padding-right: 25px!important;
                }
                #footer_social table>tbody>tr td img {
                    width: auto!important;
                }
            }
        </style>
        <tbody><tr>
                <td align="center" style="padding-left: 5px; padding-right: 5px"><img class="confim" style="margin-right: 0!important;width: 45px" src="https://botaksign.com/wp-content/plugins/custom-botaksign/assets/images/fb.png"></td>
                <td align="center" style="padding-left: 5px; padding-right: 5px"><img class="confim" style="margin-right: 0!important;width: 45px" src="https://botaksign.com/wp-content/plugins/custom-botaksign/assets/images/inta.png"></td>
                <td align="center" style="padding-left: 5px; padding-right: 5px"><img class="confim" style="margin-right: 0!important;width: 45px" src="https://botaksign.com/wp-content/plugins/custom-botaksign/assets/images/email.png"></td>
                <td align="center" style="padding-left: 5px; padding-right: 5px"><img class="confim" style="margin-right: 0!important;width: 45px" src="https://botaksign.com/wp-content/plugins/custom-botaksign/assets/images/web.png"></td>
            </tr>
        </tbody></table></div>
<div id="footer-text" style="width:100%;text-align:center;"> 
    <span class="footer-text-1" style="font-size:10pt !important;color:#a5a6a6;">22 Yio Chu Kang, #01-34 Highland Centre, Singapore 545535 | 6286 2298 | www.botaksign.com</span><br>
    <span class="footer-text-2" style="color:#27793d;font-size:10pt !important;font-family:segoe-bold;">Terms &amp; Conditions | Privacy Policy | Update Preferences</span><br>
    <span class="footer-text-3" style="font-size:10pt !important;color:#a5a6a6;">© Botak Sign Pte Ltd 2020. All rights reserved.</span>
</div>